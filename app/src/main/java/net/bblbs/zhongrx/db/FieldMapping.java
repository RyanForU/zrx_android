package net.bblbs.zhongrx.db;

/**
 * Created by qxb on 2017/4/25.
 */

public class FieldMapping {
    private String basicField;
    private String additionalField;

    public FieldMapping(String basicField, String additionalField){
        this.basicField =  basicField;
        this.additionalField = additionalField;
    }

    public String getBasicField() {
        return basicField;
    }

    public void setBasicField(String basicField) {
        this.basicField = basicField;
    }

    public String getAdditionalField() {
        return additionalField;
    }

    public void setAdditionalField(String additionalField) {
        this.additionalField = additionalField;
    }
}
