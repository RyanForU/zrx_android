package net.bblbs.zhongrx.db;


import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable {
    private String id;
    private String name;
    private String mobile;
    private String idNo;
    private String state;
    private String comment;
    private String createdDate;

    private String installmentVid;
    private String guaranteeVid;
    private String mortgageVid;
    private String commitmentVid;

    private String basicData;
    private String additionalData;
    private String paymentInfo;

    protected Person(Parcel in) {
        id = in.readString();
        name = in.readString();
        mobile = in.readString();
        idNo = in.readString();
        state = in.readString();
        comment = in.readString();
        createdDate = in.readString();


        installmentVid = in.readString();
        guaranteeVid = in.readString();
        mortgageVid = in.readString();
        commitmentVid = in.readString();

        basicData = in.readString();
        additionalData = in.readString();
        paymentInfo = in.readString();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getState() {
        if (state.equals("UPLOADING")){
            return "等待录入资料";
        }else if(state.equals("EXAM_SENDBACK")){
            return "内部审核-退回";
        }else
            return "潜在客户";
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public String getInstallmentVid() {
        return installmentVid;
    }

    public void setInstallmentVid(String installmentVid) {
        this.installmentVid = installmentVid;
    }

    public String getGuaranteeVid() {
        return guaranteeVid;
    }

    public void setGuaranteeVid(String guaranteeVid) {
        this.guaranteeVid = guaranteeVid;
    }

    public String getMortgageVid() {
        return mortgageVid;
    }

    public void setMortgageVid(String mortgageVid) {
        this.mortgageVid = mortgageVid;
    }

    public String getCommitmentVid() {
        return commitmentVid;
    }

    public void setCommitmentVid(String commitmentVid) {
        this.commitmentVid = commitmentVid;
    }

    public String getBasicData() {
        return basicData;
    }

    public void setBasicData(String basicData) {
        this.basicData = basicData;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Person(String id
        , String name
        , String mobile
        , String idNo
        , String state
        , String comment
        , String createdDate
        , String installmentVid
        , String guaranteeVid
        , String mortgageVid
        , String commitmentVid
        , String basicData
        , String additionalData
        , String paymentInfo
    ) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.idNo = idNo;
        this.state = state;
        this.comment = comment;
        this.createdDate = createdDate;

        this.installmentVid = installmentVid;
        this.guaranteeVid = guaranteeVid;
        this.mortgageVid = mortgageVid;
        this.commitmentVid = commitmentVid;

        this.basicData = basicData;
        this.additionalData = additionalData;
        this.paymentInfo = paymentInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(mobile);
        dest.writeString(idNo);
        dest.writeString(state);
        dest.writeString(comment);
        dest.writeString(createdDate);

        dest.writeString(installmentVid);
        dest.writeString(guaranteeVid);
        dest.writeString(mortgageVid);
        dest.writeString(commitmentVid);

        dest.writeString(basicData);
        dest.writeString(additionalData);
        dest.writeString(paymentInfo);
    }

}
