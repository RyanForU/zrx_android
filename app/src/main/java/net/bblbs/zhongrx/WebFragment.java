package net.bblbs.zhongrx;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Jay on 2015/8/28 0028.
 */
public class WebFragment extends Fragment {

    public WebFragment() {
    }

    private WebView m_wv1;
    private WebView m_wv2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_fragment, container, false);
//        WebView w = ((WebView) view.findViewById(R.id.webView));
//        w.setWebViewClient(new WebViewClient() {
////            @Override
////            public boolean shouldOverrideUrlLoading(WebView view, String url) {
////                view.loadUrl(url);
////                return true;
////            }
//        });
//        w.getSettings().setJavaScriptEnabled(true);
//        w.loadUrl("http://www.baidu.com");
////        SectionDetailActivity parentActivity = (SectionDetailActivity ) getActivity();

        initView(view);
        return view;
    }


    public void initView(View view) {
        m_wv1 = (WebView) view.findViewById(R.id.wv1);
        m_wv1.getSettings().setJavaScriptEnabled(true);
        m_wv1.loadUrl("http://www.baidu.com");
        //获取WebSettings对象,设置缩放
        m_wv1.getSettings().setUseWideViewPort(true);
        m_wv1.getSettings().setLoadWithOverviewMode(true);
        m_wv1.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                m_wv1.loadUrl(url);
                return true;
            }
        });
    }

}
