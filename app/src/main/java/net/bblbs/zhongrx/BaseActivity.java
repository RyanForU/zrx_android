package net.bblbs.zhongrx;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import net.bblbs.zhongrx.util.HttpRequest;

/**
 * Created by qxb on 2017/3/13.
 */

public class BaseActivity extends AppCompatActivity {
  private static final String TAG = "ZRX_BASE_ACTIVITY";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ActivityCollector.addActivity(this);
    //show current activity
    Log.d(TAG, getClass().getSimpleName() +":onCreate");
  }

  //session time out, go to loginActivity
  @Override
  protected void onResume() {
    super.onResume();
    if ( HttpRequest.mTime==0
        ||(System.currentTimeMillis() - HttpRequest.mTime) > 30 * 60 * 1000){

      HttpRequest.mTime = 0;
      HttpRequest.mToken = null;
      HttpRequest.mHeaderName = null;
      HttpRequest.mSession = null;

      ActivityCollector.finishAll();
      Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
      startActivity(intent);

//
//      AlertDialog.Builder builder = new AlertDialog.Builder(this);
//      builder.setTitle("Warning");
//      builder.setMessage("会话已超时,请重新登陆");
//      builder.setCancelable(false);
//      builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
//        @Override
//        public void onClick(DialogInterface dialog, int which) {
//          ActivityCollector.finishAll();
//          Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
//          startActivity(intent);
//        }
//      });
//      builder.show();

//      Intent intent = new Intent(this, LoginActivity.class);
//      startActivity(intent);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    ActivityCollector.removeActivity(this);
  }
}
