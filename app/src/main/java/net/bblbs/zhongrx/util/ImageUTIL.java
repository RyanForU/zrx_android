package net.bblbs.zhongrx.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileOutputStream;

/**
 * Created by liuji on 2017/4/12.
 */

public class ImageUTIL {
    static  BitmapFactory.Options options = new BitmapFactory.Options();
    static{
        options.inSampleSize = calculateInSampleSize(options, 2000, 2828);
        options.inJustDecodeBounds = false;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }
    public static void compressImg(String path){
        try {
            Bitmap bitmap =BitmapFactory.decodeFile(path,options);
            FileOutputStream os = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, os);
        }catch (Exception e){

        }
    }

    public static void compressImg(Bitmap srcBitmap,String out){
        try {
            FileOutputStream os = new FileOutputStream(out);
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 60, os);
        }catch (Exception e){

        }
    }
}
