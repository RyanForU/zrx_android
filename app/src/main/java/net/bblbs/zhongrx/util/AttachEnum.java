package net.bblbs.zhongrx.util;

/**
 * Created by qxb on 2017/3/2.
 */

public enum  AttachEnum {

//    ID_CARD_FRONT_IMG("身份证正面照片"),
//    ID_CARD_BACK_IMG("身份证反面照片"),
//    RESIDENCE_BOOKLET_1_IMG("户口本照片_1"),
//    RESIDENCE_BOOKLET_2_IMG("户口本照片_2"),
//
//    RESIDENCE_BOOKLET_3_IMG("户口本照片_3"),
//    RESIDENCE_BOOKLET_4_IMG("户口本照片_4"),
//    MARITAL_STATUS_IMG("婚姻证明照片"),
//    HOUSE_PROPERTY_IMG("房产证明照片"),
//
//    EARNING_IMG("收入证明照片"),
//    DISTRICT_IMG("小区名称照片"),
//    BUILDING_IMG("单元外观照片"),
//    BUILDING_NO_IMG("单元楼幢号照片"),
//
//    HOUSE_NO_IMG("门牌号照片"),
//    INDOOR_IMG("进门全景照片"),
//    GROUP_IMG("客户合影照片"),

    //required
    INSTALLMENT_VID("卡分期业务申请表-视频"),
    GUARANTEE_CONTRACT_VID("担保合同签字-借款人-视频"),
    MORTGAGE_CONTRACT_VID("抵押合同签字-借款人-视频"),
    COMMITMENT_LETTER_VID("共同还款承诺书签字-正式担保人-视频"),

    GUARANTEE_CONTRACT_MATE_VID("担保合同签字-配偶-视频"),
    MORTGAGE_CONTRACT_MATE_VID("抵押合同签字-配偶-视频");

    private String value;

    public static int OFFSET =1000;

    private AttachEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
