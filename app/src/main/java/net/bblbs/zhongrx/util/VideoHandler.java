package net.bblbs.zhongrx.util;

import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;

import net.bblbs.zhongrx.SectionListActivity;

import java.io.File;

/**
 * Created by qxb on 2017/4/19.
 */

public class VideoHandler implements FFmpegExecuteResponseHandler {

    private String oldFilePath;
    private String newFilePath;

    public VideoHandler(String oldFilePath, String newFilePath){
        this.oldFilePath = oldFilePath;
        this.newFilePath = newFilePath;
    }

    @Override
    public void onSuccess(String message) {
        File oldFile = new File(oldFilePath);
        oldFile.delete();

        File newFile = new File(newFilePath);
        newFile.renameTo(new File(newFilePath.substring(0, newFilePath.length()-4)));

    }

    @Override
    public void onProgress(String message) {

    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onStart() {
    }

    @Override
    public void onFinish() {

    }
}
