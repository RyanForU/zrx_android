package net.bblbs.zhongrx.util;

/**
 * Created by qixubin on 26/02/2017.
 */

public interface HttpCallbackListener {
    void onFinish(String response);

    void onError(Exception e);
}
