package net.bblbs.zhongrx.util;

import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import net.bblbs.zhongrx.ZRXApplication;

/**
 * Created by liuji on 2017/4/13.
 */

public class VideoUTIL {
    static FFmpeg ffmpeg = FFmpeg.getInstance(ZRXApplication.getAppContext());
    static{
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {}

                @Override
                public void onFailure() {}

                @Override
                public void onSuccess() {}

                @Override
                public void onFinish() {}
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }
    }

    public static FFmpeg getFfmpeg() {
        return ffmpeg;
    }

    static class DefaultFFmpegExecuteResponseHandler implements FFmpegExecuteResponseHandler  {
        @Override
        public void onStart() {

        }

        @Override
        public void onFinish() {

        }

        @Override
        public void onSuccess(String message) {

            Log.d("video ffmpeg onSuccess", message);
        }

        @Override
        public void onProgress(String message) {
            Log.d("video ffmpeg onProgress", message);
        }

        @Override
        public void onFailure(String message) {
            Log.d("video ffmpeg onFailure", message);
        }
    }

//    public static void compressVideo(String src,String tar,FFmpegExecuteResponseHandler handler){
//        if(handler==null)
//            handler=new DefaultFFmpegExecuteResponseHandler();
//        Log.d("video","ffmpeg start");
//        //.\ffmpeg.exe -i temp.mp4 -vcodec libx264 -preset medium -crf 35 -y -vf "scale=1920:-1" -acodec libmp3lame -an a.mp4
//        //String []cmd={/*"-vcodec libx264",*/"-preset medium","-crf 35","-y","-vf \"scale=1920:-1\"","-acodec libmp3lame","-i "+src,tar};
//        //String []cmd={"-version"};
//        String []cmd={"-i",src,"-vcodec","libx264","-preset","medium","-crf","35","-y","-acodec","libmp3lame",tar};
//        try {
//            VideoUTIL.getFfmpeg().execute(cmd, handler);
//        }catch (Exception e){
//            Log.d("video ffmpeg Exception", e.getMessage());
//        }
//
//        //Log.d("video","ffmpeg end");
//    }
}
