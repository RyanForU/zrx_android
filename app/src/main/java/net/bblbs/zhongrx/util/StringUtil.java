package net.bblbs.zhongrx.util;

/**
 * Created by qxb on 2017/4/26.
 */

public class StringUtil {

    public static int getNamesLength(String names){

        String[] nameArray = names.split("\\|", -1);
        if (nameArray.length == 1 && nameArray[0].trim().equals("")){
            return 0;
        }

        return nameArray.length;
    }

    public static int getIndexOfArray(String names, String name){
        String[] nameArray = names.split("\\|", -1);
        int result = -1;
        for(int i = 0 ; i < nameArray.length; i++){
            if (name.equals(nameArray[i])){
                result = i;
            }
        }
        return result;
    }

    public static String getMaxIndexName(String names, String name){
        String[] nameArray = names.split("\\|", -1);
        if (nameArray.length ==1 && nameArray[0].equals("")){
            return name;
        }

        int maxIndex =0;
        boolean exists= false;
        for(int i = 0 ; i < nameArray.length; i++){
            String currName = nameArray[i];
            if (currName.equals(name)){
                exists = true;
                continue;
            }else if(currName.contains(name)){
                exists = true;
                int index  =  new Integer(currName.substring(name.length()));
                if (index > maxIndex){
                    maxIndex = index;
                }
            }
        }

        if (exists){
            return name + int2str(maxIndex+1);
        }else{
            return name;
        }

    }

    public static String int2str(int i){
        if(i <=9){
            return "0"+ i ;
        }else{
            return "" + i;
        }
    }

}
