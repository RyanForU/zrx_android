package net.bblbs.zhongrx.util;

/**
 * Created by qxb on 2017/3/3.
 */

public enum StateEnum {
    //get RISK_EXAM_PASS  REVIEWING, REVIEW_PASS, REVIEW_FAIL, REVIEW_SENDBACK, and then only RISK_EXAM_PASS and REVIEW_SENDBACK can be editted
    //and sendback comments is stored at "comment" field

    TARCUSTOM("潜在客户"),
    UPLOADING("等待录入资料"),
    EXAMING("内部审核中"),

    EXAM_PASS("内部审核-通过"),
    EXAM_FAIL("内部审核-未通过"),
    EXAM_SENDBACK("内部审核-退回"),

    EXT_EXAM_PASS("外部审核-通过"),
    EXT_EXAM_FAIL("外部审核-未通过"),
    EXT_EXAM_SENDBACK("外部审核-退回"),

    GPS_INSTALLING("GPS安装&放车"),
    FAIL("未通过/异常"),
    INSTALL_SENDBACK("电核-退回"),
    DONE("完成（结案）");

    private String value;

    private StateEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
