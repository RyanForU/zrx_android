package net.bblbs.zhongrx.util;

import android.text.TextUtils;

import net.bblbs.zhongrx.db.FieldMapping;
import net.bblbs.zhongrx.db.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by qxb on 2017/4/18.
 */

public class JsonUtil {

    public static String put(String json, String key, String value){
        String result="";

        try {
            JSONObject obj = null;
            if (TextUtils.isEmpty(json)){
                obj = new JSONObject();
                obj.put(key, value);
                result = obj.toString();
            }else{
                JSONObject additionalJson = new JSONObject(json);
                additionalJson.put(key, value);
                result = additionalJson.toString();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isChanged(String oldJson, String newJson){
        if (TextUtils.isEmpty(oldJson)){
            if(TextUtils.isEmpty(newJson)){
                return false;
            }else{
                return true;
            }
        }else{
            if(TextUtils.isEmpty(newJson)){
                return true;
            }else{
                try {
                    JSONObject oldObj = new JSONObject(oldJson);
                    JSONObject newObj = new JSONObject(newJson);

                    List<String> oldKeyList = copyIterator(oldObj.keys());

                    List<String> newKeyList = copyIterator(newObj.keys());

                    if (oldKeyList.size()!= newKeyList.size()){
                        return true;
                    }

                    Collections.sort(oldKeyList);
                    Collections.sort(newKeyList);
                    for(int i = 0 ; i < oldKeyList.size(); i ++){
                        String oldKey = oldKeyList.get(i);
                        String newKey = newKeyList.get(i);

                        if (!oldKey.equals(newKey)){
                            return true;
                        }

                        String oldValue = (String)oldObj.get(oldKey);
                        String newValue = (String)newObj.get(newKey);

                        if (!oldValue.equals(newValue)){
                            return true;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public static <T> List<T> copyIterator(Iterator<T> iter) {
        List<T> copy = new ArrayList<T>();
        while (iter.hasNext())
            copy.add(iter.next());
        return copy;
    }

    public static boolean contains(String json, String searchKey){
        boolean result = false;

        if (TextUtils.isEmpty(json)){
            return result;
        }

        try {
            JSONObject additionalJson = new JSONObject(json);
            Iterator<String> keys = additionalJson.keys();
            while( keys.hasNext() ) {
                String key = (String) keys.next();
                if (key.equals(searchKey)){
                    result = true;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean keyInMappingList(ArrayList<FieldMapping> list, String searchKey){
        if (list ==null){
            return false;
        }

        for(int i =0; i < list.size(); i++){
            FieldMapping mapping = list.get(i);
            if (mapping.getAdditionalField().equals(searchKey)){
                return true;
            }
        }
        return false;
    }

    public static String getValueFromXin(Person person, String searchKey){
        String result="";

        if(contains(person.getBasicData(), searchKey)){
            try {
                JSONObject basicDataObj = new JSONObject(person.getBasicData());
                return (String)basicDataObj.get(searchKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(contains(person.getPaymentInfo(), searchKey)){
            try {
                JSONObject paymentObj = new JSONObject(person.getPaymentInfo());
                return (String)paymentObj.get(searchKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

//    public static String getValueFromAdditional(Person person, String searchKey){
//        String result="";
//
//        if(contains(person.getAdditionalData(), searchKey)){
//            try {
//                JSONObject basicDataObj = new JSONObject(person.getAdditionalData());
//                return (String)basicDataObj.get(searchKey);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return result;
//    }
}
