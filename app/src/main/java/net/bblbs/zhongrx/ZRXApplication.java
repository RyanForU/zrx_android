package net.bblbs.zhongrx;

import android.app.Application;
import android.content.Context;

/**
 * Created by liuji on 2017/4/13.
 */

public class ZRXApplication extends Application {
    private static Context context;

    public void onCreate(){
        super.onCreate();
        ZRXApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return ZRXApplication.context;
    }
}
