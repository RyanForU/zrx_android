package net.bblbs.zhongrx;

import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by ardio on 2/4/2017.
 */

public class ApplicationData implements Serializable {
    private static final long serialVersionUID = 1L;
    public String id;
    public String name;

    public String getApplicantName() {
        return name;
    }

    public static String serialize(ApplicationData data) {
        if (data.id == null || data.id.equals("")) {
            Log.e("INVALID APPLICANT ID", "Invalid applicant id. No data file saved.");
            return null;//序列化失败.
        }
        String fileName = data.id + ".zrx";
        try {
            ObjectOutputStream oos;//输出流保存的文件名为  ；ObjectOutputStream能把Object输出成Byte流
            oos = new ObjectOutputStream(new FileOutputStream(fileName));
            oos.writeObject(data);
            oos.flush();//缓冲流
            oos.close(); //关闭流
            return fileName; //序列化成功
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.e("INVALID APPLICANT ID", "Serialization failure.. No data file saved.");
        return null; //序列化失败.
    }

    public static ApplicationData deserialize(String applicantID) {
        if (applicantID == null || applicantID.equals("")) {
            Log.e("INVALID APPLICANT ID", "Invalid applicant id. No data loaded.");
            return null;
        }
        String fileName = applicantID + ".zrx";
        ObjectInputStream oin = null;//局部变量必须要初始化
        try {
            oin = new ObjectInputStream(new FileInputStream(fileName));
            ApplicationData data;
            data = (ApplicationData) oin.readObject();
            return data;//由Object对象向下转型为ApplicationData对象
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
