package net.bblbs.zhongrx;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by Jay on 2015/8/28 0028.
 */
public class ApplicantFragment extends Fragment {

    private static final String TAG = "ZRX_ApplicantFragment";
    LinearLayout ll = null;
    public ApplicantFragment() {
    }

    SectionListActivity  parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.applicant_fragment, container, false);


        parent = (SectionListActivity) getActivity();
        ll= (LinearLayout) view.findViewById(R.id.ll_info);


        bindViews(view);
        return view;


    }

    private void bindViews(View view) {
        String basicData = parent.person.getBasicData();
        if(!TextUtils.isEmpty(basicData)){
            addInfo(basicData);
        }

        String paymentInfo = parent.person.getPaymentInfo();
        if(!TextUtils.isEmpty(paymentInfo)){
            addInfo(paymentInfo);
        }
    }

    private void addInfo(String json){
        try {
            JSONObject basicJson = new JSONObject(json);
            Iterator<String> keys = basicJson.keys();

            ArrayList<String> list = getSortedList(keys);
           for(String key : list) {

                String value = (String) basicJson.get(key);

                Log.d(TAG, "onCreateView: ");

                LinearLayout ll_child = new LinearLayout(parent);
                ll_child.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                ll_child.setOrientation(LinearLayout.HORIZONTAL);
                ll_child.setPadding(0,20,0,0);

                TextView tvKey = new TextView(parent);
                tvKey.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                tvKey.setText(key);
                tvKey.setSaveEnabled(false);

                ll_child.addView(tvKey);


                if (key.contains("外部照片")){
                    ImageView iv = new ImageView(parent);
                    iv.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f));
                    Glide.with(parent).load(value).into(iv);
                    ll_child.addView(iv);
                }else{

                    TextView tvValue = new TextView(parent);
                    tvValue.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f));
                    tvValue.setText(value);
                    tvValue.setSaveEnabled(false);
                    tvValue.setPadding(30,0,0,0);
                    if (value.contains("http")){
                        Linkify.addLinks(tvValue, Linkify.WEB_URLS);
                    }
                    ll_child.addView(tvValue);
                }
                ll.addView(ll_child);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> getSortedList(Iterator<String> iterator){
        ArrayList<String> list = new ArrayList<String>();
        while( iterator.hasNext() ) {
            list.add(iterator.next());
        }
        Collections.sort(list);
        return list;
    }
}
