package net.bblbs.zhongrx.view;

import android.app.Activity;
import android.app.Dialog;

import android.content.Context;
import android.content.Intent;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import net.bblbs.zhongrx.DynamicPhotoFragment;
import net.bblbs.zhongrx.Photo1Fragment;
import net.bblbs.zhongrx.Photo2Fragment;
import net.bblbs.zhongrx.Photo3Fragment;
import net.bblbs.zhongrx.Photo4Fragment;
import net.bblbs.zhongrx.util.AttachEnum;


/**
 * Created by qxb on 2017/4/20.
 */

public class MyImageView extends android.support.v7.widget.AppCompatImageView {


    GestureDetector gestureDetector;

    int index =-1;
    Fragment fragment = null;

    public MyImageView(Context context) {
        this(context, null);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        gestureDetector = new GestureDetector(context, new GestureListener(context));
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

    }

    public void setFragment(Fragment fragment){
        this.fragment = fragment;
    }

    public void setIndex(int index){
        this.index = index;
    }


//    @Override
//    public void onClik

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return gestureDetector.onTouchEvent(e);
    }
//
//
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private Context pContext;

        public GestureListener(Context context){
            pContext= context;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
//            Toast.makeText( getContext(),"single Tap" , Toast.LENGTH_SHORT).show();
            return true;
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);


//            AppCompatActivity activity = (AppCompatActivity)pContext;

            if (fragment instanceof Photo1Fragment){
                Photo1Fragment newfragment = (Photo1Fragment) fragment;
                newfragment.mCurrentMyImageView = MyImageView.this;
                newfragment.mCurrentIndex = index;

                newfragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
            }else if(fragment instanceof Photo2Fragment){
                Photo2Fragment newfragment = (Photo2Fragment) fragment;
                newfragment.mCurrentMyImageView = MyImageView.this;
                newfragment.mCurrentIndex = index;
                newfragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
            }else if(fragment instanceof Photo3Fragment){
                Photo3Fragment newfragment = (Photo3Fragment) fragment;
                newfragment.mCurrentMyImageView = MyImageView.this;
                newfragment.mCurrentIndex = index;
                newfragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
            }else if(fragment instanceof Photo4Fragment){
                Photo4Fragment newfragment = (Photo4Fragment) fragment;
                newfragment.mCurrentMyImageView = MyImageView.this;
                newfragment.mCurrentIndex = index;
                newfragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);
            }else if(fragment instanceof DynamicPhotoFragment){
                DynamicPhotoFragment newfragment = (DynamicPhotoFragment) fragment;
                newfragment.mCurrentMyImageView = MyImageView.this;
                newfragment.mCurrentIndex = index;
                newfragment.startActivityForResult(Intent.createChooser(intent, "Select Picture"), 2);

            }


            return true;
        }
    }


}