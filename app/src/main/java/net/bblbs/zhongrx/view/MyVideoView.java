package net.bblbs.zhongrx.view;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import android.widget.VideoView;

import net.bblbs.zhongrx.SectionListActivity;
import net.bblbs.zhongrx.util.AttachEnum;
import net.bblbs.zhongrx.util.FileStoreUtil;

/**
 * Created by qxb on 2017/3/17.
 */

public class MyVideoView extends VideoView {

  private int mVideoWidth;
  private int mVideoHeight;

  GestureDetector gestureDetector;

  int ordinal;

  public MyVideoView(Context context, AttributeSet attrs) {
    super(context, attrs);

    gestureDetector = new GestureDetector(context, new MyVideoView.GestureListener(context, this));
    this.setOnTouchListener(new OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
      }
    });
  }

  public MyVideoView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  public MyVideoView(Context context) {
    super(context);
  }

  public void setVideoSize(int width, int height) {
    mVideoWidth = width;
    mVideoHeight = height;
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    // Log.i("@@@", "onMeasure");
    int width = getDefaultSize(mVideoWidth, widthMeasureSpec);
    int height = getDefaultSize(mVideoHeight, heightMeasureSpec);
    if (mVideoWidth > 0 && mVideoHeight > 0) {
      if (mVideoWidth * height > width * mVideoHeight) {
        // Log.i("@@@", "image too tall, correcting");
        height = width * mVideoHeight / mVideoWidth;
      } else if (mVideoWidth * height < width * mVideoHeight) {
        // Log.i("@@@", "image too wide, correcting");
        width = height * mVideoWidth / mVideoHeight;
      } else {
        // Log.i("@@@", "aspect ratio is correct: " +
        // width+"/"+height+"="+
        // mVideoWidth+"/"+mVideoHeight);
      }
    }
    // Log.i("@@@", "setting size: " + width + 'x' + height);
    setMeasuredDimension(width, height);
  }

  public void setOrdinal(int i){
    this.ordinal = i;
  }

  private class GestureListener extends GestureDetector.SimpleOnGestureListener {
    private Context mContext;
    private MyVideoView mVideoView;
    public GestureListener(Context context, MyVideoView myVideoView){
      mContext= context;
      mVideoView = myVideoView;
    }

    @Override
    public boolean onDown(MotionEvent e) {
      return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
//            Toast.makeText( getContext(),"single Tap" , Toast.LENGTH_SHORT).show();
      if (mVideoView.isPlaying()){
        mVideoView.pause();
      }else{
        mVideoView.start();
      }

      return true;
    }

    // event when double tap occurs
    @Override
    public boolean onDoubleTap(MotionEvent e) {
      SectionListActivity activity = (SectionListActivity) mContext;

      Intent intent = new Intent();
      intent.setType("video/*");
      intent.setAction(Intent.ACTION_GET_CONTENT);


//      AppCompatActivity activity = (AppCompatActivity) mContext;
      Fragment fragment = null;


      fragment = activity.getSupportFragmentManager().findFragmentByTag("视频资料");
      fragment.startActivityForResult(Intent.createChooser(intent, "Select Video"), AttachEnum.OFFSET+ordinal);


      return true;
    }
  }
}