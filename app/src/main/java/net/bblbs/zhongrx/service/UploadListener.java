package net.bblbs.zhongrx.service;

/**
 * Created by qxb on 2017/3/21.
 */

public interface UploadListener {

  void onFinish();

  void onFailed();

//  void onSuccess();
}
