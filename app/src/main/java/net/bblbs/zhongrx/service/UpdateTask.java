package net.bblbs.zhongrx.service;

import android.os.AsyncTask;
import android.util.Log;

import net.bblbs.zhongrx.util.HttpRequest;

import static net.bblbs.zhongrx.SectionListActivity.person;

/**
 * Created by qxb on 2017/3/21.
 */

public class UpdateTask extends AsyncTask<String, Integer, Boolean> {
  private static final String TAG = "ZRX_UPDATETASK";

  public static final int TYPE_SUCCESS = 0;
  public static final int TYPE_FAILED = 1;

  private UploadListener listener;

  private int lastProgress;

  public UpdateTask(UploadListener listener){
    this.listener = listener;
  }

  @Override
  protected Boolean doInBackground(String... params) {

    boolean result;

    String baseUrl = params[0];
    String json = params[1];

    Log.d(TAG, "doInBackground: " + baseUrl + "/n" + json);

    return HttpRequest.serviceUpdate(baseUrl, "/tango/uploadAddtionalData/" + person.getId(), json);
  }

  @Override
  protected void onProgressUpdate(Integer... values) {
//    int progress = values[0];
//    if (progress > lastProgress) {
//      listener.onProgress(progress);
//      lastProgress = progress;
//    }
  }

  @Override
  protected void onPostExecute(Boolean result) {
    if (result) {
      listener.onFinish();
    } else {
      listener.onFailed();
    }
  }

}
