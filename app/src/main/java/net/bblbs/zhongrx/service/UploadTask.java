package net.bblbs.zhongrx.service;

import android.os.AsyncTask;
import android.util.Log;

import net.bblbs.zhongrx.R;
import net.bblbs.zhongrx.util.HttpCallbackListener;
import net.bblbs.zhongrx.util.HttpRequest;

import static net.bblbs.zhongrx.SectionListActivity.person;

/**
 * Created by qxb on 2017/3/21.
 */

public class UploadTask extends AsyncTask<String, Integer, Boolean> {
  private static final String TAG = "ZRX_UPLOADTASK";

  public static final int TYPE_SUCCESS = 0;
  public static final int TYPE_FAILED = 1;

  private UploadListener listener;

  private int lastProgress;

  public UploadTask(UploadListener listener){
    this.listener = listener;
  }

  @Override
  protected Boolean doInBackground(String... params) {

    boolean result;

    String baseUrl = params[0];
    String uploadFileName = params[1];
    String uploadFileURL = params[2];
    Log.d(TAG, "doInBackground: " + baseUrl + "/n" + uploadFileName + "/n" + uploadFileURL);

    return HttpRequest.serviceUpload(baseUrl, "/tango/uploadFiles/" + person.getId(), uploadFileName, uploadFileURL);
  }

  @Override
  protected void onProgressUpdate(Integer... values) {
//    int progress = values[0];
//    if (progress > lastProgress) {
//      listener.onProgress(progress);
//      lastProgress = progress;
//    }
  }

  @Override
  protected void onPostExecute(Boolean result) {
    if (result) {
      listener.onFinish();
    } else {
      listener.onFailed();
    }
  }

}
