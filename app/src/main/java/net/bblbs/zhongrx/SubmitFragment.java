package net.bblbs.zhongrx;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import net.bblbs.zhongrx.util.AttachEnum;
import net.bblbs.zhongrx.util.FileStoreUtil;

import org.w3c.dom.Text;

/**
 * Created by Jay on 2015/8/28 0028.
 */
public class SubmitFragment extends Fragment implements View.OnClickListener{

    private Button submit_btn;
    private TextView mTVVideoCompressing;


    private String submitList = "";
    SectionListActivity  parent;


    public SubmitFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.submit_fragment, container, false);
//        TextView txt_content = (TextView) view.findViewById(R.id.textView);
//        txt_content.setText("Submit Fragment");
        Log.e("提交资料", "上传申请人资料");

        parent = (SectionListActivity) getActivity();
        parent.submitList = "";
        parent.additionalDataChanged = false;

//        String response = HttpRequest.request("http://" + getActivity().getString(R.string.web_service_server) + ":80/auth/csrf", "", "GET");
//        Log.e("HEHE", response);

        bindViews(view);
        return view;
    }

    private void bindViews(View view) {
        submit_btn = (Button) view.findViewById(R.id.submit_button);
        submit_btn.setOnClickListener(this);

        mTVVideoCompressing = (TextView) view.findViewById(R.id.tv_video_compress);


        if(parent.person.getState().equals("潜在客户")){
            mTVVideoCompressing.setText("该用户状态不能提交资料");
            submit_btn.setEnabled(false);
            return;
        }


//        if(FileStoreUtil.existsTmpFile(parent.person.getId(), parent)){
//            mTVVideoCompressing.setText("视频压缩中,请稍后尝试上传资料");
//            submit_btn.setEnabled(false);
//            return;
//        }else{
//            mTVVideoCompressing.setText("");
//        }

        if (parent.canUpload()){
            submit_btn.setEnabled(true);
        }else{
            submit_btn.setEnabled(false);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
//                Toast.makeText(parent, "click...", Toast.LENGTH_LONG).show();

                final ConnectivityManager conMgr = (ConnectivityManager) parent.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.isConnected()) {

                } else {
                    Toast.makeText(parent, "没有网络连接", Toast.LENGTH_LONG).show();
                    break;
                }
                new AlertDialog.Builder(parent)
                    .setTitle("确认提交")
                    .setMessage("确认提交以下资料?\n " + parent.submitList)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            parent.upload();
                            submit_btn.setEnabled(false);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();


                break;
            default:
                break;
        }
    }
}
