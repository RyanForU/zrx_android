package net.bblbs.zhongrx;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class SectionContent {

    public static final String KEY_FRAGMENT = "SECTION FRAGMENT";

    /**
     * An array of section items.
     */
    public static final List<SectionItem> ITEMS = new ArrayList<>();


//    /**
//     * A map of section items, by ID.
//     */
//    public static final Map<String, SectionItem> ITEM_MAP = new HashMap<String, SectionItem>();

    static {
        // 创建申请表各部分HukoubenFragment
        addItem(createSectionItem(1,"申请人基本资料", "申请人基本资料", new ApplicantFragment()));
        addItem(createSectionItem(2,"补充资料", "补充资料", new ApplicantAdditionalFragment()));
        addItem(createSectionItem(3,"借款人资料照片", "借款人资料照片",new Photo1Fragment()));
        addItem(createSectionItem(4,"担保人资料照片", "担保人资料照片",new Photo2Fragment()));
        addItem(createSectionItem(5,"合同照片", "合同照片",new Photo3Fragment()));
        addItem(createSectionItem(6,"借款人住所照片", "借款人住所照片",new Photo4Fragment()));
        addItem(createSectionItem(7,"视频资料", "视频资料",new Video1Fragment()));
        addItem(createSectionItem(8,"补充其他照片", "补充其他照片",new DynamicPhotoFragment()));
//        addItem(createSectionItem(6,"网页", "打开指定网站",new WebFragment()));
        addItem(createSectionItem(9,"提交资料", "上传申请资料",new SubmitFragment()));
    }

    public static Fragment getSectionFragmentByPosition(int position){
        return ITEMS.get(position-1).sectionFragment;
    }

    public static String getSectionFragmentTag(int position){
        return ITEMS.get(position-1).description;
    }

    private static void addItem(SectionItem item) {
        ITEMS.add(item);
//        ITEM_MAP.put(item.id, item);
    }

    private static SectionItem createSectionItem(int position, String content, String description, Fragment sectionFragment) {
        return new SectionItem(String.valueOf(position), content, description, sectionFragment);
    }

//    private static String makeDetails(int position) {
//        StringBuilder builder = new StringBuilder();
//        builder.append("Details about Item: ").append(position);
//        for (int i = 0; i < position; i++) {
//            builder.append("\nMore description information here.");
//        }
//        return builder.toString();
//    }

    /**
     * A section item representing a section.
     */
    public static class SectionItem {
        public final String id;
        public final String content;
        public final String description;
        public final Fragment sectionFragment;

        public SectionItem(String id, String content, String description, Fragment sectionFragment) {
            this.id = id;
            this.content = content;
            this.description = description;
            this.sectionFragment = sectionFragment;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
