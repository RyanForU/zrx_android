package net.bblbs.zhongrx;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import net.bblbs.zhongrx.util.AttachEnum;
import net.bblbs.zhongrx.util.FileStoreUtil;
import net.bblbs.zhongrx.util.VideoHandler;
import net.bblbs.zhongrx.util.VideoUTIL;
import net.bblbs.zhongrx.view.MyVideoView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Jay on 2015/8/28 0028.
 */
public class Video1Fragment extends Fragment implements View.OnClickListener{


    private static final String TAG = "ZRX_SIGN_VIDEO";



    private MyVideoView mVVInstallment;
    private Button mBtnInstallment;
    private MyVideoView mVVGuarantee;
    private Button mBtnGuarantee;
    private MyVideoView mVVMortgage;
    private Button mBtnMortgage;
    private MyVideoView mVVCommitment;
    private Button mBtnCommitment;

    private MyVideoView mVVMateGuarantee;
    private Button mBtnMateGuarantee;
    private MyVideoView mVVMateMortgage;
    private Button mBtnMateMortgage;


    private String mCurrInstallmentVid = "";
    private String mPrevInstallmentVid = "";
    private String mCurrGuaranteeVid = "";
    private String mPrevGuaranteeVid = "";
    private String mCurrMortgageVid = "";
    private String mPrevMortgageVid = "";
    private String mCurrCommitmentVid = "";
    private String mPrevCommitmentVid = "";

    private String mCurrGuaranteeMateVid = "";
    private String mPrevGuaranteeMateVid = "";
    private String mCurrMortgageMateVid = "";
    private String mPrevMortgageMateVid = "";


    SectionListActivity  parent;
    private String personId;

    public Video1Fragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_1_fragment,container,false);
//        TextView txt_content = (TextView) view.findViewById(R.id.txt_content);
//        txt_content.setText("第四个Fragment");
        Log.d(TAG, "卡分期视频拍摄页");

        parent = (SectionListActivity) getActivity();
        personId = parent.person.getId();

        mPrevInstallmentVid = parent.attachFiles[0];
        mPrevGuaranteeVid = parent.attachFiles[1];
        mPrevMortgageVid = parent.attachFiles[2];
        mPrevCommitmentVid = parent.attachFiles[3];

        mPrevGuaranteeMateVid = parent.attachFiles[4];
        mPrevMortgageMateVid = parent.attachFiles[5];

        bindViews(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_installment:
                dispatchTakeVideoIntent(AttachEnum.INSTALLMENT_VID);
                break;
            case R.id.btn_guarantee:
                dispatchTakeVideoIntent(AttachEnum.GUARANTEE_CONTRACT_VID);
                break;
            case R.id.btn_mortgage:
                dispatchTakeVideoIntent(AttachEnum.MORTGAGE_CONTRACT_VID);
                break;
            case R.id.btn_commitment:
                dispatchTakeVideoIntent(AttachEnum.COMMITMENT_LETTER_VID);
                break;

            case R.id.btn_mate_guarantee:
                dispatchTakeVideoIntent(AttachEnum.GUARANTEE_CONTRACT_MATE_VID);
                break;
            case R.id.btn_mate_mortgage:
                dispatchTakeVideoIntent(AttachEnum.MORTGAGE_CONTRACT_MATE_VID);
                break;


            default:
                break;
        }
    }

    private void bindViews(View view) {
        mVVInstallment = (MyVideoView) view.findViewById(R.id.video_installment);
        mVVInstallment.setOrdinal(AttachEnum.INSTALLMENT_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevInstallmentVid)){
            bindVideoViewAndFile(mVVInstallment, mPrevInstallmentVid, null);
        }

        mVVGuarantee = (MyVideoView) view.findViewById(R.id.video_guarantee);
        mVVGuarantee.setOrdinal(AttachEnum.GUARANTEE_CONTRACT_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevGuaranteeVid)){
            bindVideoViewAndFile(mVVGuarantee, mPrevGuaranteeVid, null);
        }

        mVVMortgage = (MyVideoView) view.findViewById(R.id.video_mortgage);
        mVVMortgage.setOrdinal(AttachEnum.MORTGAGE_CONTRACT_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevMortgageVid)){
            bindVideoViewAndFile(mVVMortgage, mPrevMortgageVid, null);
        }

        mVVCommitment = (MyVideoView) view.findViewById(R.id.video_commitment);
        mVVCommitment.setOrdinal(AttachEnum.COMMITMENT_LETTER_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevCommitmentVid)){
            bindVideoViewAndFile(mVVCommitment, mPrevCommitmentVid, null);
        }

        mVVMateGuarantee = (MyVideoView) view.findViewById(R.id.video_mate_guarantee);
        mVVMateGuarantee.setOrdinal(AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevGuaranteeMateVid)){
            bindVideoViewAndFile(mVVMateGuarantee, mPrevGuaranteeMateVid, null);
        }

        mVVMateMortgage = (MyVideoView) view.findViewById(R.id.video_mate_mortgage);
        mVVMateMortgage.setOrdinal(AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal());

        if (!TextUtils.isEmpty(mPrevMortgageMateVid)){
            bindVideoViewAndFile(mVVMateMortgage, mPrevMortgageMateVid, null);
        }


        mBtnInstallment = (Button) view.findViewById(R.id.btn_installment);
        mBtnInstallment.setOnClickListener(this);

        mBtnGuarantee = (Button) view.findViewById(R.id.btn_guarantee);
        mBtnGuarantee.setOnClickListener(this);

        mBtnMortgage = (Button) view.findViewById(R.id.btn_mortgage);
        mBtnMortgage.setOnClickListener(this);

        mBtnCommitment = (Button) view.findViewById(R.id.btn_commitment);
        mBtnCommitment.setOnClickListener(this);

        mBtnMateGuarantee = (Button) view.findViewById(R.id.btn_mate_guarantee);
        mBtnMateGuarantee.setOnClickListener(this);

        mBtnMateMortgage = (Button) view.findViewById(R.id.btn_mate_mortgage);
        mBtnMateMortgage.setOnClickListener(this);

    }

    private void dispatchTakeVideoIntent(AttachEnum actionCode) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        switch(actionCode) {
            case INSTALLMENT_VID:
                File installmentFile = null;

                try {
                    installmentFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.INSTALLMENT_VID.ordinal(), parent);
                    mCurrInstallmentVid = installmentFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(installmentFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    installmentFile = null;
                }
                break;

            case GUARANTEE_CONTRACT_VID:
                File guaranteeFile = null;

                try {
                    guaranteeFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.GUARANTEE_CONTRACT_VID.ordinal(), parent);
                    mCurrGuaranteeVid = guaranteeFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(guaranteeFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    guaranteeFile = null;
                }
                break;

            case MORTGAGE_CONTRACT_VID:
                File mortgageFile = null;

                try {
                    mortgageFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.MORTGAGE_CONTRACT_VID.ordinal(), parent);
                    mCurrMortgageVid = mortgageFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mortgageFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    mortgageFile = null;
                }
                break;

            case COMMITMENT_LETTER_VID:
                File commitmentFile = null;

                try {
                    commitmentFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.COMMITMENT_LETTER_VID.ordinal(), parent);
                    mCurrCommitmentVid = commitmentFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(commitmentFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    commitmentFile = null;
                }
                break;

            case GUARANTEE_CONTRACT_MATE_VID:
                File guaranteeMateFile = null;

                try {
                    guaranteeMateFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal(), parent);
                    mCurrGuaranteeMateVid = guaranteeMateFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(guaranteeMateFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    guaranteeMateFile = null;
                }
                break;

            case MORTGAGE_CONTRACT_MATE_VID:
                File mortgageMateFile = null;

                try {
                    mortgageMateFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal(), parent);
                    mCurrMortgageMateVid = mortgageMateFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mortgageMateFile));
                } catch (IOException e) {
                    e.printStackTrace();
                    mortgageMateFile = null;
                }
                break;

            default:
                break;
        } // switch

        startActivityForResult(takePictureIntent, actionCode.ordinal());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == AttachEnum.INSTALLMENT_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File installmentFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    installmentFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.INSTALLMENT_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(installmentFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >= 50){
                        installmentFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }

                    mCurrInstallmentVid = installmentFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVInstallment, mCurrInstallmentVid, mPrevInstallmentVid);
                    mPrevInstallmentVid = mCurrInstallmentVid;
                    parent.attachFiles[AttachEnum.INSTALLMENT_VID.ordinal()] = mCurrInstallmentVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }

        }
        if (requestCode == AttachEnum.INSTALLMENT_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVInstallment, mCurrInstallmentVid, mPrevInstallmentVid);
                mPrevInstallmentVid = mCurrInstallmentVid;
                parent.attachFiles[AttachEnum.INSTALLMENT_VID.ordinal()] = mCurrInstallmentVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrInstallmentVid !=null){
                    File file = new File(mCurrInstallmentVid);
                    file.delete();
                }
            }
        }


        if (requestCode == AttachEnum.GUARANTEE_CONTRACT_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File guaranteeFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    guaranteeFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.GUARANTEE_CONTRACT_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(guaranteeFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >= 50){
                        guaranteeFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }

                    mCurrGuaranteeVid = guaranteeFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVGuarantee, mCurrGuaranteeVid, mPrevGuaranteeVid);
                    mPrevGuaranteeVid = mCurrGuaranteeVid;
                    parent.attachFiles[AttachEnum.GUARANTEE_CONTRACT_VID.ordinal()] = mCurrGuaranteeVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }

        }
        if (requestCode == AttachEnum.GUARANTEE_CONTRACT_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVGuarantee, mCurrGuaranteeVid, mPrevGuaranteeVid);
                mPrevGuaranteeVid = mCurrGuaranteeVid;
                parent.attachFiles[AttachEnum.GUARANTEE_CONTRACT_VID.ordinal()] = mCurrGuaranteeVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrGuaranteeVid !=null){
                    File file = new File(mCurrGuaranteeVid);
                    file.delete();
                }
            }
        }


        if (requestCode == AttachEnum.MORTGAGE_CONTRACT_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File mortgageFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    mortgageFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.MORTGAGE_CONTRACT_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(mortgageFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >= 50){
                        mortgageFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }


                    mCurrMortgageVid = mortgageFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVMortgage, mCurrMortgageVid, mPrevMortgageVid);
                    mPrevMortgageVid = mCurrMortgageVid;
                    parent.attachFiles[AttachEnum.MORTGAGE_CONTRACT_VID.ordinal()] = mCurrMortgageVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
        }


        if (requestCode == AttachEnum.MORTGAGE_CONTRACT_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVMortgage, mCurrMortgageVid, mPrevMortgageVid);
                mPrevMortgageVid = mCurrMortgageVid;
                parent.attachFiles[AttachEnum.MORTGAGE_CONTRACT_VID.ordinal()] = mCurrMortgageVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrMortgageVid !=null){
                    File file = new File(mCurrMortgageVid);
                    file.delete();
                }
            }
        }

        if (requestCode == AttachEnum.COMMITMENT_LETTER_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File commitmentFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    commitmentFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.COMMITMENT_LETTER_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(commitmentFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >=  50){
                        commitmentFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }


                    mCurrCommitmentVid = commitmentFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVCommitment, mCurrCommitmentVid, mPrevCommitmentVid);
                    mPrevCommitmentVid = mCurrCommitmentVid;
                    parent.attachFiles[AttachEnum.COMMITMENT_LETTER_VID.ordinal()] = mCurrCommitmentVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
        }

        if (requestCode == AttachEnum.COMMITMENT_LETTER_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVCommitment, mCurrCommitmentVid, mPrevCommitmentVid);
                mPrevCommitmentVid = mCurrCommitmentVid;
                parent.attachFiles[AttachEnum.COMMITMENT_LETTER_VID.ordinal()] = mCurrCommitmentVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrCommitmentVid !=null){
                    File file = new File(mCurrCommitmentVid);
                    file.delete();
                }
            }
        }


        if (requestCode == AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File guaranteeMateFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    guaranteeMateFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(guaranteeMateFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >= 50){
                        guaranteeMateFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }

                    mCurrGuaranteeMateVid = guaranteeMateFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVMateGuarantee, mCurrGuaranteeMateVid, mPrevGuaranteeMateVid);
                    mPrevGuaranteeMateVid = mCurrGuaranteeMateVid;
                    parent.attachFiles[AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal()] = mCurrGuaranteeMateVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }

        }
        if (requestCode == AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVMateGuarantee, mCurrGuaranteeMateVid, mPrevGuaranteeMateVid);
                mPrevGuaranteeMateVid = mCurrGuaranteeMateVid;
                parent.attachFiles[AttachEnum.GUARANTEE_CONTRACT_MATE_VID.ordinal()] = mCurrGuaranteeMateVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrGuaranteeMateVid !=null){
                    File file = new File(mCurrGuaranteeMateVid);
                    file.delete();
                }
            }
        }


        if (requestCode == AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal() + + AttachEnum.OFFSET) {
            if (resultCode == Activity.RESULT_OK ) {
                File mortgageMateFile = null;
                try {
                    InputStream is = ZRXApplication.getAppContext().getContentResolver().openInputStream(intent.getData());
                    mortgageMateFile = FileStoreUtil.setUpVideoFile(personId,AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal(),parent);
                    FileOutputStream os=new FileOutputStream(mortgageMateFile);
                    double len = FileStoreUtil.copyFile(is, os);
                    if (len >= 50){
                        mortgageMateFile.delete();
                        Toast.makeText(parent, "文件大小大于50M,请重新选择", Toast.LENGTH_LONG).show();
                        return;
                    }


                    mCurrMortgageMateVid = mortgageMateFile.getAbsolutePath();
                    bindVideoViewAndFile(mVVMateMortgage, mCurrMortgageMateVid, mPrevMortgageMateVid);
                    mPrevMortgageMateVid = mCurrMortgageMateVid;
                    parent.attachFiles[AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal()] = mCurrMortgageMateVid;
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            }
        }


        if (requestCode == AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal()) {
            if (resultCode == Activity.RESULT_OK ){
                bindVideoViewAndFile(mVVMateMortgage, mCurrMortgageMateVid, mPrevMortgageMateVid);
                mPrevMortgageMateVid = mCurrMortgageMateVid;
                parent.attachFiles[AttachEnum.MORTGAGE_CONTRACT_MATE_VID.ordinal()] = mCurrMortgageMateVid;
            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrMortgageMateVid !=null){
                    File file = new File(mCurrMortgageMateVid);
                    file.delete();
                }
            }
        }

    }

    private void bindVideoViewAndFile(VideoView videoView, String filePath, String deleteFilePath) {
        Log.d(TAG, "bindVideoViewAndFile: ");

        if (deleteFilePath !=null){
            File file = new File(deleteFilePath);
            boolean deleted = file.delete();
            Log.d(TAG, "delete file: "+ deleteFilePath);

//            File compressFile = new File(deleteFilePath + ".mp4");
//            boolean compressDeleted = compressFile.delete();
//            Log.d(TAG, "delete compress file: "+ deleteFilePath);
        }


        Uri uri = Uri.parse(filePath);
        videoView.setVideoURI(uri);
        videoView.start();
    }
}
