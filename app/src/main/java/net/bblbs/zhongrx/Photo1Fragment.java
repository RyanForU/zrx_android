package net.bblbs.zhongrx;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.provider.MediaStore;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.bumptech.glide.Glide;
import net.bblbs.zhongrx.util.FileStoreUtil;
import net.bblbs.zhongrx.util.ImageUTIL;
import net.bblbs.zhongrx.util.StringUtil;
import net.bblbs.zhongrx.view.MyImageView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Photo1Fragment extends Fragment implements View.OnClickListener{
    private static final String TAG = "ZRX_Applicant";
    public static final String SP_NAME = "fragment1_photo";

    private ArrayList<String> mCurrImgList = new ArrayList<>();
    private ArrayList<String> mPrevImgList = new ArrayList<>();

    String personId;
    SectionListActivity  parent;

    FloatingActionButton addIV = null;
    LinearLayout ll = null;

    public MyImageView mCurrentMyImageView=null;
    public int mCurrentIndex = -1;

    public Photo1Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "进入页面初始化");
        View view = inflater.inflate(R.layout.photo_1_fragment, container, false);


        parent = (SectionListActivity) getActivity();
        personId = parent.person.getId();

        ll= (LinearLayout) view.findViewById(R.id.ll_photo_1);

        addIV = (FloatingActionButton) view.findViewById(R.id.fab_add_photo_1);
        addIV.setOnClickListener(this);

        mPrevImgList = parent.mImgList1;

        bindViews(view);
        return view;
    }

    private void bindViews(View view) {
        SharedPreferences pref = parent.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);

        String names = pref.getString(SP_NAME + "_" + personId, "");
        String[] nameArray = names.split("\\|", -1);

        if (nameArray.length ==1 && nameArray[0].trim().equals("")) {
            return;
        }
        for(int i  = 0; i < mPrevImgList.size() ; i++){
            mCurrImgList.add("");
        }

        for (int i =0; i < nameArray.length; i++) {

            MyImageView newIV = new MyImageView(parent);
            newIV.setFragment(this);
            newIV.setIndex(i);
            newIV.setBackgroundResource(R.drawable.id_photo);
            newIV.setLayoutParams(new LinearLayout.LayoutParams(550,400));
            newIV.setScaleType(ImageView.ScaleType.FIT_XY);
            ll.addView(newIV);

            if (!TextUtils.isEmpty(mPrevImgList.get(i))){
                Glide.with(parent).load(mPrevImgList.get(i)).into(newIV);
            }


            Button imageButton = new Button(parent);
            imageButton.setText(nameArray[i]);
            imageButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            imageButton.setOnClickListener(this);
            ll.addView(imageButton);
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            mCurrentMyImageView = (MyImageView) ll.getChildAt(ll.indexOfChild(v) -1);
            dispatchTakePictureIntent(((Button) v).getText().toString());
        }
        switch (v.getId()) {
            case R.id.fab_add_photo_1:

                AlertDialog.Builder alert = new AlertDialog.Builder(parent);
//                final EditText edittext = new EditText(parent);
                alert.setTitle("添加照片");
                alert.setMessage("请选择照片名称");

                final Spinner spinner = new Spinner(parent);
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(  parent,
                    R.array.applicant, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinner.setAdapter(adapter);

                alert.setView(spinner);
                alert.setPositiveButton("确定", null);
                alert.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });


                final AlertDialog alertDialog = alert.create();
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String imageName = spinner.getSelectedItem().toString();


                        SharedPreferences pref = parent.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
                        String names = pref.getString(SP_NAME + "_" + personId,"");



                        String newImageName = StringUtil.getMaxIndexName(names, imageName);


                        String newNames= "";
                        if(names.equals("")){
                            newNames =newImageName;
                        }else{
                            newNames = names +"|" + newImageName;
                        }

                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString(SP_NAME + "_" + personId, newNames);
                        editor.apply();

                        MyImageView newIV = new MyImageView(parent);
                        newIV.setFragment(Photo1Fragment.this);
                        newIV.setIndex(StringUtil.getNamesLength(names));
                        newIV.setBackgroundResource(R.drawable.id_photo);
                        newIV.setLayoutParams(new LinearLayout.LayoutParams(550,400));
                        newIV.setScaleType(ImageView.ScaleType.FIT_XY);
                        ll.addView(newIV);


                        Button imageButton = new Button(parent);
                        imageButton.setText(newImageName);
                        imageButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        imageButton.setOnClickListener(Photo1Fragment.this);
                        ll.addView(imageButton);



                        mCurrImgList.add("");
                        mPrevImgList.add("");

                        alertDialog.dismiss();
                    }
                });

                break;
            default:
                break;
        }
    }

    private void dispatchTakePictureIntent(String buttonName) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        SharedPreferences pref = parent.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE);
        String names = pref.getString(SP_NAME + "_" + personId,"");
        int index = StringUtil.getIndexOfArray(names, buttonName);
        if (index ==-1) {
            return;
        }

        mCurrentIndex = index;

        File imageFile = null;
        try {
            imageFile = FileStoreUtil.setUpPhotoFile(personId, SP_NAME + "_" + index, parent);
            mCurrImgList.set(index ,imageFile.getAbsolutePath());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));

        } catch (IOException e) {
            Log.e(TAG, "dispatchTakePictureIntent: DISTRICT_IMG: "+ e.toString() );
            imageFile = null;
        }


        startActivityForResult(takePictureIntent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);


        if (requestCode ==2){

            if (resultCode == Activity.RESULT_OK ) {
                try {
                    File tmpFile = null;
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(ZRXApplication.getAppContext().getContentResolver(), intent.getData());
                    tmpFile = FileStoreUtil.setUpPhotoFile(personId, SP_NAME+ "_" + mCurrentIndex, parent);
                    ImageUTIL.compressImg(bitmap, tmpFile.getAbsolutePath());
                    mCurrImgList.set(mCurrentIndex ,tmpFile.getAbsolutePath());

                    FileStoreUtil.deleteOldFile(mPrevImgList.get(mCurrentIndex));
                    Glide.with(parent).load(mCurrImgList.get(mCurrentIndex)).into(mCurrentMyImageView);
                    mPrevImgList.set(mCurrentIndex, mCurrImgList.get(mCurrentIndex));


                }catch(IOException e){
                    Log.d(TAG, "onActivityResult: "+ e.toString());
                }


            }

        }else if (requestCode ==1){
            if (resultCode == Activity.RESULT_OK ) {

                FileStoreUtil.deleteOldFile(mPrevImgList.get(mCurrentIndex));
                ImageUTIL.compressImg(mCurrImgList.get(mCurrentIndex));
                Glide.with(parent).load(mCurrImgList.get(mCurrentIndex)).into(mCurrentMyImageView);
                mPrevImgList.set(mCurrentIndex, mCurrImgList.get(mCurrentIndex));

            }

            if (resultCode == Activity.RESULT_CANCELED){
                if (mCurrImgList.get(mCurrentIndex) !=null){
                    File file = new File(mCurrImgList.get(mCurrentIndex));
                    file.delete();
                }
            }
        }



    }
}
